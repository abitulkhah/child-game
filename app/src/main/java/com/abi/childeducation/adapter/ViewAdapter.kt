package com.abi.childeducation.adapter

import android.graphics.BitmapFactory
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.abi.childeducation.R
import com.abi.childeducation.model.Study
import android.view.animation.AnimationUtils
import kotlinx.android.synthetic.main.item_cardview.view.*


class ViewAdapter(private val list:ArrayList<Study>, val listener: (Study) -> Unit): RecyclerView.Adapter<ViewAdapter.ViewHolder>(){

    var lastPosition = -1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewAdapter.ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_cardview, parent,false))
    }

    override fun getItemCount(): Int {
        return list?.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(list[position], listener)
        setAnimation(holder.view, position)
    }


    override fun onViewDetachedFromWindow(holder: ViewHolder) {
        super.onViewDetachedFromWindow(holder)
        holder.view.clearAnimation()
    }

    class ViewHolder (val view: View) : RecyclerView.ViewHolder(view) {
        fun bind(Study: Study, listener: (Study) -> Unit) = with(view) {

            titleId.setText(Study?.title)
            val imageResource = resources.getIdentifier(Study?.image, "drawable", context.packageName)
            imageId.setImageBitmap(BitmapFactory.decodeResource(resources,imageResource ))

            if(Study?.title==null){
                titleId.visibility = View.GONE
            }
            setOnClickListener {
                val animation = AnimationUtils.loadAnimation(context, R.anim.zoom_out_animation)
                view.cardView.startAnimation(animation)
                listener(Study)
            }
        }
    }

    private fun setAnimation(viewToAnimate: View, position: Int) {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition) {
            val animation = AnimationUtils.loadAnimation(viewToAnimate.context, if (position > lastPosition)
                R.anim.up_from_bottom
            else
                R.anim.down_from_top)
            viewToAnimate.startAnimation(animation)
            lastPosition = position
        }
    }

    private fun singleAnimation(viewToAnimate: View, position: Int) {
        val animation = AnimationUtils.loadAnimation(viewToAnimate.context, R.anim.zoom_in_animation)
        viewToAnimate.startAnimation(animation)
    }
}