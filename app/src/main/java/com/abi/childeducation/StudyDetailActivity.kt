package com.abi.childeducation

import android.content.Intent
import android.media.MediaPlayer
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.view.Menu
import android.view.MenuItem
import com.abi.childeducation.adapter.ViewAdapter
import com.abi.childeducation.model.Study
import com.google.gson.Gson
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import kotlinx.android.synthetic.main.recycler_view.*
import java.io.IOException

class StudyDetailActivity : AppCompatActivity() {

    var mp = MediaPlayer()
    private var isGridlayout = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.recycler_view)
        val title = intent.getStringExtra("studyName")
        this.setTitle(title)
        val gson = Gson()
        val listData = gson.fromJson(loadJSONFromAsset(title.toLowerCase()+"_list"), Array<Study>::class.java)
        var mLayoutManager = GridLayoutManager(this, 4)

        recyclerviewId.setHasFixedSize(true)
        recyclerviewId.layoutManager = mLayoutManager
        val listMenu = listData.toCollection(ArrayList())

        val adapter = ViewAdapter(listMenu){

            if(mp.isPlaying){
                mp.stop()
                mp.release()
            }
            val soundName = resources.getIdentifier(it.soundName, "raw", this.packageName)
            mp = MediaPlayer.create(this, soundName)
            mp.setVolume(100.0F,100.0F)
            mp.start()
        }

        recyclerviewId.adapter = adapter
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.change_view, menu)
        return super.onCreateOptionsMenu(menu)
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.changeView -> {
                isGridlayout = !isGridlayout
                var mLayoutManager =
                    if (isGridlayout)
                        GridLayoutManager(this, 2)
                    else
                        LinearLayoutManager(this)

                recyclerviewId.setHasFixedSize(true)
                recyclerviewId.layoutManager = mLayoutManager
                return true
            }

            R.id.changeLanguange -> {
                val intent = Intent(this, SettingActivity::class.java)
                startActivity(intent)
                return true
            }
        }
        false
        return super.onOptionsItemSelected(item)
    }

    fun loadJSONFromAsset(filename: String): JsonElement {

        var jsonElement: JsonElement? = null

        try {
            val inpuStream = this.assets.open(filename)
            val size = inpuStream?.available() as Int
            val buffer = ByteArray(size)
            inpuStream.read(buffer)
            inpuStream.close()
            val json = String(buffer, Charsets.UTF_8)
            jsonElement = JsonParser().parse(json).asJsonObject.get("data")
        } catch (ex: IOException) {
            ex.printStackTrace()
            return JsonObject()
        }

        return jsonElement
    }
}