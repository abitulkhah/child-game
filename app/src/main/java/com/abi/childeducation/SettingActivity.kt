package com.abi.childeducation

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import kotlinx.android.synthetic.main.setting.*

class SettingActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.setting)
        setTitle("Pengaturan")

        val languangeList = arrayOf("Indonesia", "Inggris")
        spinnerLanguange.adapter = ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, languangeList)
        spinnerLanguange.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
//                Toast.makeText(this@SettingActivity, languangeList[p2], Toast.LENGTH_LONG).show()
            }

        }
    }
}