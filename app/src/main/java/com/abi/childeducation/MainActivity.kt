package com.abi.childeducation

import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v4.app.Fragment
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.widget.FrameLayout
import com.abi.childeducation.fragment.AboutFragment
import com.abi.childeducation.fragment.ExerciseFragment
import com.abi.childeducation.fragment.StudyFragment
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private var content: FrameLayout? = null

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_study -> {
                val studyFragment = StudyFragment.newInstance()
                openFragment(studyFragment)
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_exercise -> {
                val exerciseFragment = ExerciseFragment.newInstance()
                openFragment(exerciseFragment)
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_about -> {
                val aboutFragment = AboutFragment.newInstance()
                openFragment(aboutFragment)
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        navigation.selectedItemId = R.id.navigation_study;
    }

    private fun openFragment(fragment: Fragment) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.content, fragment)
        transaction.commit()
    }

    override fun onBackPressed() {
        val builder = AlertDialog.Builder(this@MainActivity)
        builder.setTitle("Info")
        builder.setMessage("Anda yakin keluar aplikasi ?")
        builder.setPositiveButton("Ya"){dialog,which ->
            super.onBackPressed()
        }

        builder.setNegativeButton("Tidak"){dialog,which ->
            dialog.dismiss()
        }
        // Finally, make the alert dialog using builder
        val dialog: AlertDialog = builder.create()

        // Display the alert dialog on app interface
        dialog.show()
    }
}


