package com.abi.childeducation.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.abi.childeducation.R

class ExerciseFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        activity?.setTitle("Latihan")
        return inflater?.inflate(R.layout.recycler_view, container, false)
    }

    companion object {
        fun newInstance(): ExerciseFragment {
            return ExerciseFragment()
        }
    }
}