package com.abi.childeducation.fragment

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import com.abi.childeducation.adapter.ViewAdapter
import com.abi.childeducation.R
import kotlinx.android.synthetic.main.recycler_view.*
import android.support.v7.widget.GridLayoutManager
import android.media.MediaPlayer
import android.support.v7.widget.LinearLayoutManager
import android.view.*
import com.abi.childeducation.SettingActivity
import com.abi.childeducation.StudyDetailActivity
import com.abi.childeducation.model.Study
import com.google.gson.Gson
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import java.io.IOException
import com.google.gson.JsonParser
import kotlin.collections.ArrayList


class StudyFragment : Fragment() {

    private var mp = MediaPlayer()

    private var isGridlayout = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        activity?.setTitle("Belajar")
        return inflater?.inflate(R.layout.recycler_view, container, false)
    }


    companion object {
        fun newInstance(): StudyFragment {
            return StudyFragment()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.change_view, menu);
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.changeView -> {
                isGridlayout = !isGridlayout
                var mLayoutManager =
                    if (isGridlayout)
                        GridLayoutManager(context, 2)
                    else
                        LinearLayoutManager(context)

                recyclerviewId.setHasFixedSize(true)
                recyclerviewId.layoutManager = mLayoutManager
                return true
            }

            R.id.changeLanguange -> {
                val intent = Intent(context, SettingActivity::class.java)
                requireActivity().startActivity(intent)
                return true
            }
        }
        false
        return super.onOptionsItemSelected(item)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        super.onViewCreated(view, savedInstanceState)

        val gson = Gson()
        val listData = gson.fromJson(loadJSONFromAsset("menu_list"), Array<Study>::class.java)
        var mLayoutManager = GridLayoutManager(context, 2)

        recyclerviewId.setHasFixedSize(true)
        recyclerviewId.layoutManager = mLayoutManager
        val listMenu = listData.toCollection(ArrayList())
        val adapter = ViewAdapter(listMenu){

           if(mp.isPlaying){
                mp.stop()
                mp.release()
            }

            val soundName = resources.getIdentifier(it.soundName, "raw", context?.packageName)
            mp = MediaPlayer.create(context, soundName)
            mp.setVolume(100.0F,100.0F)
            mp.start()

            val intent = Intent(context,StudyDetailActivity::class.java)
            intent.putExtra("studyName",it.title)

            requireActivity().startActivity(intent)

        }

        recyclerviewId.adapter = adapter
    }

    fun loadJSONFromAsset(filename: String): JsonElement {

        var jsonElement: JsonElement? = null

        try {
            val inpuStream = context?.assets?.open(filename)
            val size = inpuStream?.available() as Int
            val buffer = ByteArray(size)
            inpuStream.read(buffer)
            inpuStream.close()
            val json = String(buffer, Charsets.UTF_8)
            jsonElement = JsonParser().parse(json).asJsonObject.get("data")
        } catch (ex: IOException) {
            ex.printStackTrace()
            return JsonObject()
        }

        return jsonElement
    }
}